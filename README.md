# Latex in Adobe Illustrator Script

This script allows for LaTex text and equations to be inserted in Adobe Illustrator.

This script is based of a version by [mkuznets](https://github.com/mkuznets/latex-illustrator). 

This fork contains the following improvements:
- Compatible with MacOS;
- Easy editing of the LaTex text;
- Re-editing text in case of LaTex error.
- Support for Adobe Illustrator 2023 and later.

## Installation
### Windows

#### Installing Script
Make sure that Adobe Illustrator is closed. Download `GenerateLatex.js` and copy it to `%programfiles%\Adobe\Adobe Illustrator 2023\Presets\en_US\Scripts` (or something similar depending on your install location and version of Adobe Illustrator).

Make sure that both pdflatex and GhostScript are able to run from the command prompt. This can be checked by opening command prompt (`cmd.exe`) or `Terminal` (on Windows 11) and running `pdflatex.exe -version` and `mgs.exe -version` and checking if information about pdflatex and Ghostscript is printed, respectively.

### MacOS
#### Installing Script
Due to read/write permissions in MacOS. Some extra steps need to be followed in order to get the script working. Make sure that Adobe Illustrator is closed before performing these steps.
1. Open terminal;
2. Run `mkdir illustratortemp`;
3. Run `pwd|pbcopy` (to copy the current path to your clipboard);
4. Run `cd illustratortemp`;
5. Run `open .`, and finder will open the folder;
6. Download `latex2illustrator.sh` and place it in the folder.
7. Open `latex2illustrator.sh` with a text editor and replace all instances of `{USER-FOLDER}` with the copied path.
8. In terminal, run `chmod +x latex2illustrator.sh`;
9. Run again `pwd|pbcopy` (to copy the current path to your clipboard);
10. In finder, right-click `latex2illustrator.sh`, click `Get-Info`, and make sure that `Open with:` says `Terminal` (otherwise select it);
11. Download `GenerateLatex.js` and open it in a text-editor. Then, on line 11, paste the copied path in between the apostrophes. (Such that the line reads something like `var tempPath = "/Users/{user}/illustratortemp";`); 
12. Copy `GenerateLatex.js` to `/Applications/Adobe Illustrator 2022/Presets/en_US/Scripts`.

## Usage
To execute the script, go to `File`, `Scripts` and then click `GenerateLaTex`. A textbox will appear in which you can enter you LaTex code. 

To edit the LateX text, select the text object and run the script again. The current LaTex code will be pre-filled in the textbox, which you can then edit.

### Setting up a hotkey
To setup an hotkey to execute the script more easily:

1. In Illustrator go to `Window` and then `Actions` (and a new window will appear);
2. Click the `[+]` button ('Create New Action') in the bottom right;
3. For `Name` fill-in `LaTex Script` and for `Function Key` select the desired hotkey to use;
4. Click `Record`;
5. Click the `◼️` button ('Stop Recording') in the bottom-right;
6. Select the `LaTex Script` row, then click `≡` in the top-right, then `Insert Menu Item...`;
7. In the text window type `GenerateLaTex` and then click `Find:` and then click `OK`;
8. Now you can close the Actions window and use the hotkey to run the script.
