//------------------//
//   Main script    //
//------------------//
try {
    if (app.documents.length > 0) {  // Check if a document is open
        // Variables
        platform = getPlatform();

        // Platform specific stuff
        if (platform == "mac") {
            var tempPath = "";  // PASTE THE COPIED PATH HERE

            // Check if temppath has been set
            if (tempPath == "") {
                throw new Error('Temporary path not set. See the README on https://gitlab.com/patrickkoelewijn/latex-illustrator.');
            }

            var fileSep = "/";
        }
        else if (platform == "win") {
            var tempPath = Folder.temp.fsName;    // Temporary folder path
            var indexTemp = tempPath.indexOf("Temporary Internet Files");
            if (indexTemp >= 0) tempPath = tempPath.substr(0, indexTemp + 4);

            var fileSep = "\\";
        }

        var tempFileName = "latex2illustrator"; // File name of temporary files
        var defaultCode = "$$";   // Default textbox input

        // Check if the selected item already has latex code attached to it
        var selectedItems = app.activeDocument.selection;
        var tagIndex = -1;
        // check if objects are selected
        if (selectedItems.length > 0) {
            // select first object in selection (could be changed to also loop through items)
            var selectedItem = selectedItems[0];
            var tagList = selectedItem.tags;
            // if the object has tags, check if it has the latex code tag
            if (tagList.length > 0) {
                for (i = 0; i < tagList.length; i++) {
                    if (tagList[i].name == "LatexCode") {
                        // retrieve latex code of object
                        tagText = tagList[i].value;
                        defaultCode = tagText;
                        tagIndex = i;
                        break;
                    }
                }
            }
        }
        // prompt for user input
        var latexCode = promptAndCompile(defaultCode, tempPath, tempFileName, platform, fileSep);

        if (latexCode != null) {
            // Check if successfully compiled and place in active doc
            checkAndPlace(selectedItems, tempPath, latexCode, tagIndex, tempFileName, fileSep);
        }
    }
    else {
        throw new Error('No active document!');
    }
}
catch (e) {
    alert(e.message, "Script Alert", true);
}

//---------------//
//   Functions   //
//---------------//
// Prompt the user for latex code and compile the code
function promptAndCompile(lastCode, tempPath, tempFileName, platform, fileSep) {
    var latexCode = prompt("Please enter LaTeX code", lastCode, "LaTeX");
    if (latexCode != null) {
        // add latex header etc. to create a complete latex document
        var latexFile = new File(tempPath + fileSep + tempFileName + "-temp.tex");
        latexFile.open("w");
        latexFile.writeln("\\documentclass[varwidth=true]{standalone}");
        // add or remove additional latex packages here
        latexFile.writeln("\\usepackage{amsmath}");
        latexFile.writeln("\\usepackage{amssymb}");
        latexFile.writeln("\\usepackage{gensymb}");   // for \degree
        latexFile.writeln("\\usepackage{textcomp}");  // for \textdegree
        latexFile.writeln("\\usepackage{euscript}");  // for \EuScript 
        latexFile.writeln("\\usepackage{bm}");        // bold math
        latexFile.writeln("\\begin{document}");
        latexFile.writeln("\\pagestyle{empty}"); // no page number
        latexFile.writeln(latexCode);
        latexFile.writeln("\\end{document}");
        latexFile.close();

        // Remove old files
        var pdffile = File(tempPath + fileSep + tempFileName + ".pdf");
        var pdfAux = File(tempPath + fileSep + tempFileName + ".aux");
        if (pdffile.exists) {
            pdffile.remove();
        }
        if (pdfAux.exists) {
            pdfAux.remove();
        }

        // create a batch file calling latex
        if (platform == "mac") {
            var extension = ".sh";
            var pdflatexpath = "pdflatex";
        }
        else if (platform == "win") {
            var extension = ".bat";
            var pdflatexpath = "pdflatex.exe";
            var ghostscriptpath = "mgs.exe";
        }

        var batchfile = new File(tempPath + fileSep + tempFileName + extension);

        if (platform == "mac") {
            if (batchfile.exists) {
                batchfile.execute();
            }
            else {
                alert("See the README on https://gitlab.com/patrickkoelewijn/latex-illustrator.")
                latexCode = null;
            }
        }
        else if (platform == "win") {
            batchfile.open("w");
            if (batchfile.exists) {
                batchfile.writeln(pdflatexpath + ' -aux-directory="' + tempPath + '" -include-directory="' + tempPath + '" -output-directory="' + tempPath + '" "' + tempPath + fileSep + tempFileName + '-temp.tex"');
                batchfile.writeln(ghostscriptpath + ' -q -dNOPAUSE -dBATCH -dSAFER -dNoOutputFonts -sDEVICE=pdfwrite -sOutputFile="' + tempPath + fileSep + tempFileName + '.pdf" "' + tempPath + fileSep + tempFileName + '-temp.pdf"')
                batchfile.writeln("exit");
                batchfile.close();
                batchfile.execute();
            }
            else {
                alert("Error creating pdflatex script.")
                latexCode = null;
            }
        }
    }
    return latexCode;
}

// Check if the pdffile exists
function checkAndPlace(selectedItems, tempPath, latexCode, tagIndex, tempFileName, fileSep) {
    // Wait till aux file exists (pdf is created right after)
    var pdffile = File(tempPath + fileSep + tempFileName + ".pdf");
    var pdfAux = File(tempPath + fileSep + tempFileName + ".aux");
    for (i = 0; i < 5000; i++) {
        $.sleep(1);
        if (pdfAux.exists) {
            break;
        }
    }

    // If pdf file exists place it in the document
    if (pdffile.exists) {
        placeLatexPdfFile(selectedItems, pdffile, latexCode, tagIndex);
    }
    else if (pdfAux.exists) { // If aux file exists probably latex error
        alert("File " + tempPath + fileSep + pdffile.name + " could not be created. LaTeX error?");
        var latexCode = promptAndCompile(latexCode, tempPath, tempFileName);
        if (latexCode != null) {
            checkAndPlace(selectedItems, tempPath, latexCode, tagIndex, tempFileName);
        }
    }
    else { // Error in compiling latex 
        alert("Could not compile Latex code. Check log file in " + tempPath)
    }
}

// Place the pdf-file in the current active document
function placeLatexPdfFile(selectedItems, pdffile, latexcode, tagIndex) {
    // import pdf file into the current document
    var grp = app.activeDocument.activeLayer.groupItems.createFromFile(pdffile);
    // The imported objects are grouped twice. Now move the subgroup
    // items to the main group and skip the last item which is the page frame
    for (var i = grp.pageItems[0].pageItems.length; --i >= 0;) {
        grp.pageItems[0].pageItems[i].move(grp, ElementPlacement.PLACEATEND);
    }
    var last = grp.pageItems.length - 1;
    if (last >= 0 && grp.pageItems[last].typename == 'PathItem')
        grp.pageItems[last].remove();

    // if an object is selected move new object to the old objects location and remove old object
    if (selectedItems.length > 0) {
        var selectedItem = selectedItems[0];
        var tagList = selectedItem.tags;
        if (tagIndex != -1) {
            if (tagList[tagIndex].name = "LatexCode") { // Should always be the case
                grp.translate(selectedItem.pageItems[0].position[0] - grp.left, selectedItem.pageItems[0].position[1] - grp.top);
                selectedItem.remove();
            }
        }
    }
    else { // else move the (new) object to middle of view
        grp.translate(app.activeDocument.activeView.centerPoint[0] - grp.left, app.activeDocument.activeView.centerPoint[1] - grp.top);
    }
    // Add tag to new object (with latex code)
    var tagList = grp.tags;
    var itemTag = tagList.add();
    itemTag.name = "LatexCode";
    itemTag.value = latexcode;
}

// Check temporary pathname to determine current platform
function getPlatform() {
    var pathName = Folder.temp.fsName;

    var platform = "";
    if (pathName.indexOf("/") >= 0) {
        platform = "mac";
    }
    else if (pathName.indexOf("\\") >= 0) {
        platform = "win";
    }
    else {
        throw new Error('Unrecognized platform.');
    }

    return platform;
}